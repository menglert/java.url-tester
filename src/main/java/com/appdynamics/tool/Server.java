package com.appdynamics.tool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;

import static java.lang.Thread.sleep;

@Controller
@EnableAutoConfiguration
public class Server {
    @RequestMapping("/**")
    @ResponseBody
    String all(HttpServletRequest request) throws InterruptedException {
        sleep(1500);
        return  request.getRequestURI();
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Server.class, args);
    }
}
